init: init-ci front-ready
init-ci: docker-down-clear \
	api-clear front-clear \
	docker-pull docker-build docker-up \
	api-init front-init

up: docker-up
down: docker-down
restart: down up

check: lint analyze test test-e2e
lint: api-lint front-lint
analyze: api-analyze front-analyze

test: api-test front-test
test-unit: api-test-unit
test-feature: api-test-feature
test-e2e: front-test-e2e



docker-up:
	docker-compose up -d
docker-down:
	docker-compose down --remove-orphans
docker-down-clear:
	docker-compose down -v --remove-orphans
docker-pull:
	docker-compose pull
docker-build:
	docker-compose build --pull



api-init: api-permissions api-composer-install api-all-clear api-migrations api-fixtures

api-clear:
#	docker run --rm -v ${PWD}/api:/app -w /app alpine sh -c \
#		'rm -rf $$(find storage/framework/cache/data/* -maxdepth 1 -type d)'
#	docker run --rm -v ${PWD}/api:/app -w /app alpine sh -c \
#		'rm -f $$(find storage/logs/ storage/framework/ bootstrap/cache/ -type f ! -name ".gitignore")'
api-permissions:
	#docker run --rm -v ${PWD}/api:/app -w /app alpine chmod -R 777 storage

api-cli:
	docker-compose run --rm api-php-cli /bin/bash
api-composer-install:
	docker-compose run --rm api-php-cli composer i
api-composer-update:
	docker-compose run --rm api-php-cli composer u
api-composer:
	docker-compose run --rm api-php-cli composer $(ARGS)

api-all-cache:
	docker-compose run --rm api-php-cli php artisan optimize
api-all-clear:
	docker-compose run --rm api-php-cli php artisan optimize:clear
api-cache-clear:
	docker-compose run --rm api-php-cli php artisan cache:clear
api-config-clear:
	docker-compose run --rm api-php-cli php artisan config:clear
api-migrations:
	docker-compose run --rm api-php-cli php artisan migrate
api-fixtures:
	docker-compose run --rm api-php-cli php artisan db:seed


api-check: api-lint api-analyze api-test
api-lint:
	#docker-compose run --rm api-php-cli composer lint
	#docker-compose run --rm api-php-cli composer php-cs-fixer fix -- --dry-run --diff
api-cs-fix:
	#docker-compose run --rm api-php-cli composer php-cs-fixer fix
api-analyze:
	#docker-compose run --rm api-php-cli composer psalm -- --no-diff
api-analyze-diff:
	#docker-compose run --rm api-php-cli composer psalm


api-test: api-config-clear
	docker-compose run --rm api-php-cli php artisan test
api-test-coverage: api-config-clear
	docker-compose run --rm api-php-cli php artisan test --coverage
api-test-unit: api-config-clear
	docker-compose run --rm api-php-cli php artisan test --testsuite=Unit
api-test-unit-coverage: api-config-clear
	docker-compose run --rm api-php-cli php artisan test --coverage --testsuite=Unit
api-test-feature: api-config-clear
	docker-compose run --rm api-php-cli php artisan test --testsuite=Feature
api-test-feature-coverage: api-config-clear
	docker-compose run --rm api-php-cli php artisan test --coverage --testsuite=Feature



front-clear:
	docker run --rm -v ${PWD}/frontend:/app -w /app alpine sh -c \
		'rm -rf .ready build node_modules/.vite'
front-init: front-yarn-install

front-cli:
	docker-compose run --rm frontend-node-cli sh
front-yarn-install:
	docker-compose run --rm frontend-node-cli yarn install
front-yarn-upgrade:
	docker-compose run --rm frontend-node-cli yarn upgrade

front-ready:
	docker run --rm -v ${PWD}/frontend:/app -w /app alpine touch .ready

front-check: front-lint front-analyze front-test
front-lint:
	docker-compose run --rm frontend-node-cli yarn lint
front-analyze:
	docker-compose run --rm frontend-node-cli yarn typecheck
front-eslint-fix:
	#docker-compose run --rm frontend-node-cli yarn eslint-fix
front-pretty:
	#docker-compose run --rm frontend-node-cli yarn prettier

front-test:
	docker-compose run --rm frontend-node-cli yarn test:unit
front-test-e2e:
	docker-compose run --rm frontend-node-cli yarn test:e2e
