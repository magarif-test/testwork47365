<?php

/**
 * @noinspection PhpMultipleClassDeclarationsInspection
 */

declare(strict_types=1);

namespace App\Type;

use App\Cast\Id as IdCast;
use Illuminate\Contracts\Database\Eloquent\Castable;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;
use Stringable;
use Webmozart\Assert\Assert;

final class Id implements Castable, Stringable, JsonSerializable
{
    private string $value;

    public function __construct(string $value)
    {
        Assert::uuid($value);
        $this->value = mb_strtolower($value);
    }

    public static function generate(): self
    {
        return new self(Str::uuid()->toString());
    }

    public function getValue(): string
    {
        return $this->value;
    }

    #[Pure]
    public function __toString(): string
    {
        return $this->getValue();
    }

    #[Pure]
    public function jsonSerialize(): string
    {
        return $this->getValue();
    }

    public static function castUsing(array $arguments): string
    {
        return IdCast::class;
    }
}
