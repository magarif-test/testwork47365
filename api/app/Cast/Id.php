<?php

declare(strict_types=1);

namespace App\Cast;

use App\Type\Id as IdType;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Model;
use JetBrains\PhpStorm\Pure;

final class Id implements CastsAttributes
{
    /**
     * @param Model $model
     * @param string $key
     * @param string $value
     * @param array $attributes
     *
     * @return IdType|null
     */
    public function get($model, string $key, $value, array $attributes): ?IdType
    {
        return !empty($value) ? new IdType($value) : null;
    }

    /**
     * @param Model $model
     * @param string $key
     * @param IdType $value
     * @param array $attributes
     *
     * @return string
     */
    #[Pure]
    public function set($model, string $key, $value, array $attributes): string
    {
        return $value->getValue();
    }
}
