<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Request\SomeEntityRequest;
use App\Http\Resource\SomeEntityResource;
use App\Models\SomeEntity;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\HttpFoundation\Response;

final class SomeEntityController extends Controller
{
    public function index(): JsonResource
    {
        return SomeEntityResource::collection((new SomeEntity())->simplePaginate());
    }

    #[Pure]
    public function show(SomeEntity $someEntity): JsonResource
    {
        return new SomeEntityResource($someEntity);
    }

    public function store(SomeEntityRequest $request): JsonResponse
    {
        $someEntity = (new SomeEntity())->create($request->validationData());

        return new JsonResponse(['data' => $someEntity->id], Response::HTTP_CREATED);
    }

    public function update(SomeEntity $someEntity, SomeEntityRequest $request): JsonResponse
    {
        $someEntity->update($request->validationData());

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    public function destroy(SomeEntity $someEntity): JsonResponse
    {
        $someEntity->delete();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
