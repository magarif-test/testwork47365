<?php

declare(strict_types=1);

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

final class SomeEntityRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    #[ArrayShape([
        'stringField' => 'array',
        'intField' => 'array',
        'arrayField' => 'array',
    ])]
    public function rules(): array
    {
        return [
            'stringField' => ['nullable', 'string'],
            'intField' => ['nullable', 'numeric', 'integer', 'min:0'],
            'arrayField' => ['nullable', 'array'],
        ];
    }
}
