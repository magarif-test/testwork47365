<?php

declare(strict_types=1);

namespace App\Http\Resource;

use App\Models\SomeEntity;
use App\Type\Id;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @mixin SomeEntity
 */
final class SomeEntityResource extends JsonResource
{
    /**
     * @inheritDoc
     */
    #[ArrayShape([
        'id' => Id::class,
        'stringField' => 'string',
        'intField' => 'int',
        'arrayField' => 'array',
        'created_at' => CarbonImmutable::class,
        'updated_at' => Carbon::class,
    ])]
    public function toArray($request): array
    {
        return [
            'uuid' => $this->id,
            'stringField' => $this->stringField,
            'intField' => $this->intField,
            'arrayField' => $this->arrayField,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at,
        ];
    }
}
