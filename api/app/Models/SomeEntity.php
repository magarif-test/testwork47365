<?php

declare(strict_types=1);

namespace App\Models;

use App\Type\Id;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\AsCollection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property-read Id $id
 * @property-read string $stringField
 * @property-read int $intField
 * @property-read array $arrayField
 * @property-read CarbonImmutable $created_at
 * @property-read CarbonImmutable $updated_at
 * @property-read CarbonImmutable|null $deleted_at
 *
 * @mixin Builder
 */
final class SomeEntity extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Trait\PrimaryUuid;

    protected $perPage = 10;
    protected $fillable = [
        'stringField',
        'intField',
        'arrayField',
    ];
    protected $casts = [
        'intField' => 'integer',
        'arrayField' => AsCollection::class,
    ];
}
