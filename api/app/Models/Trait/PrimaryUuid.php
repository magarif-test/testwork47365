<?php

declare(strict_types=1);

namespace App\Models\Trait;

use App\Type\Id;
use Illuminate\Database\Eloquent\Model;

/**
 * @property-read Id $id
 */
trait PrimaryUuid
{
    public static function bootPrimaryUuid(): void
    {
        static::creating(static function (Model $model): void {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Id::generate();
            }
        });
    }

    protected function initializePrimaryUuid(): void
    {
        if (! isset($this->casts[$this->getKeyName()])) {
            $this->casts[$this->getKeyName()] = Id::class;
        }
    }

    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'string';
    }
}
