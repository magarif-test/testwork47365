<?php

declare(strict_types=1);

use App\Http\Controllers\V1\SomeEntityController;
use Illuminate\Support\Facades\Route;

Route::apiResource('some-entities', SomeEntityController::class);
