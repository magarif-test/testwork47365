<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\SomeEntity;
use Illuminate\Database\Seeder;

class SomeEntitySeeder extends Seeder
{
    public function run(): void
    {
        SomeEntity::factory(10)->create();
    }
}
