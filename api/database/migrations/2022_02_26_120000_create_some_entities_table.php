<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('some_entities', static function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('stringField')->nullable();
            $table->unsignedInteger('intField')->nullable();
            $table->json('arrayField')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('some_entities');
    }
};
