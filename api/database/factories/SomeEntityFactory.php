<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\SomeEntity;
use App\Type\Id;
use Illuminate\Database\Eloquent\Factories\Factory;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @extends Factory<SomeEntity>
 */
class SomeEntityFactory extends Factory
{
    #[ArrayShape([
        'id' => Id::class,
        'stringField' => 'string',
        'intField' => 'int',
        'arrayField' => 'array',
    ])]
    public function definition(): array
    {
        return [
            'id' => Id::generate(),
            'stringField' => $this->faker->word,
            'intField' => $this->faker->randomNumber(),
            'arrayField' => [
                'stringField' => $this->faker->word,
                'intField' => $this->faker->randomNumber(),
                'arrayField' => $this->faker->words,
            ],
        ];
    }
}
