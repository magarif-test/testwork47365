<?php

declare(strict_types=1);

namespace Tests\Feature;

use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class NotFoundTest extends TestCase
{
    public function test_page_not_found(): void
    {
        $response = $this->get('/not-found');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
