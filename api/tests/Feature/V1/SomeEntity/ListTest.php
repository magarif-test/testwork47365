<?php

declare(strict_types=1);

namespace Tests\Feature\V1\SomeEntity;

use App\Models\SomeEntity;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * @internal
 */
final class ListTest extends TestCase
{
    use RefreshDatabase;

    public function test_some_entities_list(): void
    {
        /** @var Collection<SomeEntity> $models */
        $models = SomeEntity::factory($count = 5)->create();

        /** @var SomeEntity $first */
        $first = $models->first();

        $response = $this->getJson(
            route('v1.some-entities.index')
        );

        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('meta')
                    ->has('links')
                    ->has('data', $count)
                    ->has('data.0', fn ($json) =>
                        $json->where('id', $first->id->getValue())
                            ->etc()
                    )
            );
    }
}
