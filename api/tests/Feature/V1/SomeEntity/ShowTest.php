<?php

declare(strict_types=1);

namespace Tests\Feature\V1\SomeEntity;

use App\Models\SomeEntity;
use App\Type\Id;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * @internal
 */
final class ShowTest extends TestCase
{
    use RefreshDatabase;

    public function test_some_entity_show(): void
    {
        /** @var SomeEntity $model */
        $model = SomeEntity::factory()->create();

        $response = $this->getJson(
            route('v1.some-entities.show', $model)
        );

        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('data', fn ($json) =>
                    $json->where('id', $model->id->getValue())
                        ->etc()
                )
            );
    }

    public function test_some_entity_show_not_found(): void
    {
        $response = $this->getJson(
            route('v1.some-entities.show', ['some_entity' => Id::generate()->getValue()])
        );

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
