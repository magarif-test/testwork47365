<?php

declare(strict_types=1);

namespace Tests\Feature\V1\SomeEntity;

use App\Models\SomeEntity;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * @internal
 */
final class CreateTest extends TestCase
{
    use RefreshDatabase;

    public function test_some_entity_create(): void
    {
        $new = SomeEntity::factory()->make();

        $response = $this->postJson(
            route('v1.some-entities.store'),
            $new->only($new->getFillable())
        );

        $response->assertStatus(Response::HTTP_CREATED)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('data')
            );
    }
}
