<?php

declare(strict_types=1);

namespace Tests\Feature\V1\SomeEntity;

use App\Models\SomeEntity;
use App\Type\Id;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * @internal
 */
final class UpdateTest extends TestCase
{
    use RefreshDatabase;

    public function test_some_entity_update(): void
    {
        $model = SomeEntity::factory()->create();
        $new = SomeEntity::factory()->make();

        $response = $this->putJson(
            route('v1.some-entities.update', $model),
            $new->only($new->getFillable())
        );

        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    public function test_some_entity_update_not_found(): void
    {
        $new = SomeEntity::factory()->make();

        $response = $this->putJson(
            route('v1.some-entities.update', ['some_entity' => Id::generate()->getValue()]),
            $new->only($new->getFillable())
        );

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
