<?php

declare(strict_types=1);

namespace Tests\Feature\V1\SomeEntity;

use App\Models\SomeEntity;
use App\Type\Id;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * @internal
 */
final class DeleteTest extends TestCase
{
    use RefreshDatabase;

    public function test_some_entity_delete(): void
    {
        $model = SomeEntity::factory()->create();

        $response = $this->deleteJson(
            route('v1.some-entities.destroy', $model)
        );

        $response->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertModelMissing($model);
    }

    public function test_some_entity_delete_not_found(): void
    {
        $response = $this->deleteJson(
            route('v1.some-entities.destroy', ['some_entity' => Id::generate()->getValue()])
        );

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
