<?php

declare(strict_types=1);

namespace Tests\Unit\Type;

use App\Type\Id;
use Illuminate\Support\Str;
use InvalidArgumentException;
use JsonException;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Type\Id
 *
 * @internal
 */
final class IdTest extends TestCase
{
    public function test_success(): void
    {
        $id = new Id($value = Str::uuid()->toString());

        self::assertEquals($value, $id->getValue());
    }

    public function tests_case(): void
    {
        $value = Str::uuid()->toString();

        $id = new Id(mb_strtoupper($value));

        self::assertEquals($value, $id->getValue());
    }

    public function test_generate(): void
    {
        $id = Id::generate();

        self::assertNotEmpty($id->getValue());
    }

    public function test_incorrect(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Id('12345');
    }

    public function test_empty(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Id('');
    }

    public function test_to_string(): void
    {
        $id = new Id($value = Str::uuid()->toString());

        self::assertEquals($value, (string)$id);
    }

    /**
     * @throws JsonException
     */
    public function test_serialize(): void
    {
        $id = new Id($value = Str::uuid()->toString());

        self::assertEquals($value, $id->jsonSerialize());
        self::assertJson($actualJson = json_encode($id, JSON_THROW_ON_ERROR));
        self::assertJsonStringEqualsJsonString(json_encode($value, JSON_THROW_ON_ERROR), $actualJson);
    }
}
