import { defineStore, acceptHMRUpdate } from 'pinia'
import Axios, { AxiosError } from "@/api";

declare interface RegisterForm {
  name: string
  email: string
  password: string
  password_confirmation: string
}

export const useRegisterStore = defineStore({
  id: 'register',
  state: () => ({
    isRegistered: false,
    /** @type {RegisterForm} */
    form: {
      name: '',
      email: '',
      password: '',
      password_confirmation: ''
    },
    errors: {},
  }),
  getters: { },
  actions: {
    async register() {
      try {
        await Axios.get('/sanctum/csrf-cookie')
        await Axios.post('/register', this.form)
        this.isRegistered = true
      } catch (error: Error | AxiosError) {
        this.isRegistered = false
        if (Axios.isAxiosError(error)) {
          this.errors = error.response?.data.errors
        } else {
          this.errors.main = 'Unknown error'
        }
      }
    },
  },
})

// if (import.meta.hot) {
//   import.meta.hot.accept(acceptHMRUpdate(useCartStore, import.meta.hot))
// }
