import { defineStore, acceptHMRUpdate } from 'pinia'
import Axios from "@/api";

declare interface ForgotPassword {
  email: string
}
export const useForgotPasswordStore = defineStore({
  id: 'auth',
  state: () => ({
    /** @type {ForgotPassword} */
    form: {
      email: ''
    },
    errors: {}
  }),
  getters: { },
  actions: {
    async forgotPassword() {
      const response = await Axios.post('/forgot-password', this.form)
    },
    async resetPassword(
      token: string,
      email: string,
      password: string,
      password_confirmation: string
    ) {
      const response = await Axios.post('/reset-password', arguments);
      // const response = await Axios.post('/reset-password', {
      //   token: '',
      //   email: '',
      //   password: '',
      //   password_confirmation: '',
      // })
    },
    async emailVerify() {
      return Axios.post('/email/verify', {
        token: '',
        email: '',
        password: '',
        password_confirmation: '',
      })
    }
  },
})
//
// if (import.meta.hot) {
//   import.meta.hot.accept(acceptHMRUpdate(useCartStore, import.meta.hot))
// }
