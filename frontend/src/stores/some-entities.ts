import { defineStore, acceptHMRUpdate } from 'pinia'
import Axios, { v1crud as crud } from '@/api'
import SomeEntity from "@/types/SomeEntity";
import { v4 as UuidV4 } from 'uuid'

const entities = crud('some-entities')

export const useSomeEntitiesStore = defineStore({
  id: 'some-entities',
  state: () => ({
    /** @type {Array<SomeEntity>} */
    items: [],
    page: 1,
  }),
  getters: {
    pageItems(state) {
      return state.items
    },
  },
  actions: {
    async fetchByPage(page: Number = 1) {
      // this.items = await Axios.get(entities.list(), {
      //   'page': page.toString()
      // })
      // links.value = responseJson.links
      // meta.value = responseJson.meta
    },
    removeItem(item: SomeEntity) {
      SomeEntityApi.deleteSomeEntity(item)
      const i = this.items.indexOf(item)
      if (i > -1) this.items.splice(i, 1)
    },
  },
})

// if (import.meta.hot) {
//   import.meta.hot.accept(acceptHMRUpdate(useCartStore, import.meta.hot))
// }
