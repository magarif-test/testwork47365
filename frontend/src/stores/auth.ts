import { defineStore, acceptHMRUpdate } from 'pinia'
import Axios, { AxiosError } from "@/api";

declare interface LoginForm {
  email: string
  password: string
  remember: boolean
}

export const useAuthStore = defineStore({
  id: 'auth',
  state: () => ({
    /** @type {LoginForm} */
    form: {
      email: '',
      password: '',
      remember: false,
    },
    errors: {},
  }),
  getters: {
    isAuthenticated() {
      return localStorage.getItem('isAuthenticated') === 'true'
    }
  },
  actions: {
    async login() {
      try {
        await Axios.get('/sanctum/csrf-cookie')
        const response = await Axios.post('/login', this.form)
        console.log(response)
        if (response.data.two_factor) {
          this.errors.main = 'need two-factor authorization'
        } else {
          localStorage.setItem('isAuthenticated', 'true')
        }
      } catch (error: Error | AxiosError) {
        localStorage.setItem('isAuthenticated', 'false')
        if (Axios.isAxiosError(error)) {
          this.errors = error.response?.data.errors
        } else {
          this.errors.main = 'Unknown error'
        }
      }
    },
    async logout() {
      try {
        await Axios.post('/logout')
        localStorage.setItem('isAuthenticated', 'false')
      } catch (error: Error | AxiosError) {
        localStorage.setItem('isAuthenticated', 'true')
        localStorage.stItem('isAuthenticated', true)
        if (Axios.isAxiosError(error)) {
          this.errors = error.response?.data.errors
        } else {
          this.errors.main = 'Unknown error'
        }
      }
    },
  },
})

// if (import.meta.hot) {
//   import.meta.hot.accept(acceptHMRUpdate(useCartStore, import.meta.hot))
// }
