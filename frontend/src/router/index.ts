import {createRouter, createWebHistory} from 'vue-router'
import { useAuthStore } from '@/stores/auth'

//region Base
const routes: object[] = [
  {
    path: '',
    name: 'Home',
    component: () => import('@/views/HomeView.vue'),
    meta: { notRequiresAuth: true },
  },
  {
    path: "/:catchAll(.*)",
    name: "NotFound",
    component: () => import('@/views/NotFoundView.vue'),
    meta: { notRequiresAuth: true },
  },
]
//endregion Base

//region Auth
routes.push(
  {
    path: '/login',
    name: 'Auth::login',
    component: () => import('../views/Auth/LoginView.vue'),
    meta: { notRequiresAuth: true },
  },
  {
    path: '/register',
    name: 'Auth::register',
    component: () => import('../views/Auth/RegisterView.vue'),
    meta: { notRequiresAuth: true },
  },
  {
    path: '/forgot-password',
    name: 'Auth::forgot-password',
    component: () => import('../views/Auth/ForgotPasswordView.vue'),
    meta: { notRequiresAuth: true },
  },
  {
    path: '/forgot-password',
    name: 'Auth::forgot-password',
    component: () => import('../views/Auth/ForgotPasswordView.vue'),
    meta: { notRequiresAuth: true },
  }
)
//endregion Auth

//region SomeEntity
routes.push({
  path: '/some-entities',
  component: () => import('@/views/SomeEntity/MainView.vue'),
  children: [
    {
      path: '',
      name: 'SomeEntity::index',
      component: () => import('@/views/SomeEntity/IndexView.vue')
    },
    {
      path: ':id',
      name: 'SomeEntity::show',
      component: () => import('@/views/SomeEntity/ShowView.vue')
    },
    {
      path: 'new',
      name: 'SomeEntity::create',
      component: () => import('@/views/SomeEntity/EditView.vue')
    },
    {
      path: ':id',
      name: 'SomeEntity::edit',
      component: () => import('@/views/SomeEntity/EditView.vue')
    }
  ]
})
//endregion SomeEntity

// noinspection TypeScriptValidateTypes
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: routes
})

router.beforeEach(to => {
  if (
    to.name !== 'Auth::login' &&
    !to.meta.notRequiresAuth &&
    !useAuthStore().isAuthenticated
  ) {
    return { name: 'Auth::login', query: { redirect: to.fullPath } }
  }
})

export default router
