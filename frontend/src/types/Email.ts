export function isEmail(value: string): boolean {
  return true;
}

export default class {
  private readonly value: string

  constructor(value: string) {
    if (!isEmail(value)) {
      throw new TypeError()
    }
    this.value = value
  }

  toString(): string {
    return this.value
  }
}
