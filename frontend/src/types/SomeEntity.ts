import {
  v4 as UuidV4,
  parse as uuidParse,
  stringify as uuidStringify,
  validate as uuidValidate
} from 'uuid'

export default class {
  public readonly uuid: UuidV4
  public stringField: String
  public intField: Number
  public arrayField: Array<any>
  public readonly createdAt: Date
  public readonly updatedAt: Date
  public readonly deletedAt?: Date

  constructor(json: {
    uuid: String,
    stringField: String,
    intField: Number,
    arrayField: Array<any>,
    createdAt: String,
    updatedAt: String,
    deletedAt?: String
  }) {
    this.uuid = uuidParse(json.uuid)
    this.stringField = json.stringField
    this.intField = json.intField
    this.arrayField = json.arrayField
    this.createdAt = new Date(json.createdAt)
    this.updatedAt = new Date(json.updatedAt)
    this.deletedAt = json.deletedAt ? new Date(json.deletedAt) : undefined
  }

  public isActive(): Boolean {
    return !this.deletedAt
  }
}
