import {
  v4 as UuidV4,
  parse as uuidParse,
  stringify as uuidStringify,
  validate as uuidValidate
} from 'uuid'
import Email from "@/types/Email";

export default class {
  public readonly uuid: UuidV4
  public email: Email
}
