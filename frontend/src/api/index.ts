import axios, { AxiosError } from 'axios'

axios.defaults.withCredentials = true
axios.defaults.baseURL = `${import.meta.env.VITE_API_URL}`
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

// axios.defaults.maxRedirects = 0
// axios.defaults.headers.common['Accept'] = 'application/json'
// axios.defaults.headers.common['Content-Type'] = 'application/json'

export default axios
export { AxiosError }

export function v1(path: string): string {
  return '/api/v1/' + path.replace(/^\/|\/$/, '')
}

export function v1crud(path: string) {
  const base: string = v1(path)

  const empty = () => base
  const withId = id => base + '/' + id

  return {
    list: empty,
    store: empty,
    show: withId,
    update: withId,
    destroy: withId,
  }
}
