import Axios, { v1crud as crud } from '@/api'
import SomeEntity from "@/types/SomeEntity";
import { v4 as UuidV4 } from 'uuid'

const entities = crud('some-entities')

export function findSomeEntities(query?: Record<string, string>) {
  return entities.list('', query)
}

export function findSomeEntityById(uuid: UuidV4|string) {
  return entities.getJson(uuid.toString())
}

export async function createSomeEntity(item: SomeEntity) {
  return entities.postJson('', item)
}

export async function updateSomeEntity(item: SomeEntity) {
  return entities.putJson(item.uuid.toString(), item)
}

export async function deleteSomeEntity(item: SomeEntity) {
  return entities.delete(item.uuid.toString())
}
