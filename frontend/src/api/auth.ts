import Axios from '@/api/index'
import Email from "@/types/Email";

export async function register(data: Record<string, string>) {
  await Axios.get('/sanctum/csrf-cookie')
  return await Axios.post('/register', {
    name: '',
    email: '',
    password: '',
    password_confirmation: '',
  })
}

export async function login(email: Email, password: string, remember: boolean = false) {
  await Axios.get('/sanctum/csrf-cookie')
  return await Axios.post('/login', {
    email: email,
    password: password,
    remember: remember,
  })
}

export function logout() {
  return Axios.post('/logout')
}

export function forgotPassword(email: Email) {
  return Axios.post('/forgot-password', {
    email: email,
  })
}

export function resetPassword() {
  return Axios.post('/reset-password', {
    token: '',
    email: '',
    password: '',
    password_confirmation: '',
  })
}

export function emailVerify() {
  return Axios.post('/email/verify', {
    token: '',
    email: '',
    password: '',
    password_confirmation: '',
  })
}
